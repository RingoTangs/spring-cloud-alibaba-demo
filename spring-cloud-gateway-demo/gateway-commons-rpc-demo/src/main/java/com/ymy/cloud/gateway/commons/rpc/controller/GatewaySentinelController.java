package com.ymy.cloud.gateway.commons.rpc.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ringo
 * @date 2021/10/10 21:49
 */
@RestController
public class GatewaySentinelController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/hello")
    public String hello() {
        return "hello " + port;
    }
}
