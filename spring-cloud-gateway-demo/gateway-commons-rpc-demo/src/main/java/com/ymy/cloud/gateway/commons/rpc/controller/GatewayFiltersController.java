package com.ymy.cloud.gateway.commons.rpc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ringo
 * @date 2021/10/10 15:29
 */
@RestController
public class GatewayFiltersController {

    /**
     * Gateway 过滤器添加请求头
     */
    @GetMapping("/addRequestHeader")
    public String addRequestHeader(@RequestHeader("X-Request-red") String header) {
        return header;
    }


}
