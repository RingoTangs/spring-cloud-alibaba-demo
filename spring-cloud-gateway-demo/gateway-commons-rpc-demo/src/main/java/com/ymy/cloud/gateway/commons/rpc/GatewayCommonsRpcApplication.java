package com.ymy.cloud.gateway.commons.rpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ringo
 * @date 2021/10/10 15:28
 */
@SpringBootApplication
public class GatewayCommonsRpcApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayCommonsRpcApplication.class, args);
    }
}
