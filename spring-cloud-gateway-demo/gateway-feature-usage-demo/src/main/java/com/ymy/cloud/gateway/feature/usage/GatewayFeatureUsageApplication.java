package com.ymy.cloud.gateway.feature.usage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ringo
 * @date 2021/10/10 12:41
 */
@SpringBootApplication
public class GatewayFeatureUsageApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayFeatureUsageApplication.class, args);
    }
}
