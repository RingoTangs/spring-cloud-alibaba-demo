package com.ymy.cloud.gateway.quickstart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ringo
 * @date 2021/10/9 23:17
 */
@SpringBootApplication
public class GatewayQuickstartApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayQuickstartApplication.class, args);
    }
}
