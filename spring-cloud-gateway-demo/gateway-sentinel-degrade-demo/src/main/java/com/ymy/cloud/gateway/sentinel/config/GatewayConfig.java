package com.ymy.cloud.gateway.sentinel.config;

import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.BlockRequestHandler;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * Gateway 流控降级回调
 *
 * @author Ringo
 * @date 2021/10/11 10:37
 */
@Configuration
public class GatewayConfig {

    @PostConstruct
    public void init() {
        Map<String, String> map = new HashMap<>(16);
        map.put("msg", "服务降级");
        BlockRequestHandler blockRequestHandler = (ServerWebExchange exchange, Throwable t) ->
                ServerResponse.status(OK).contentType(APPLICATION_JSON).bodyValue(map);
        GatewayCallbackManager.setBlockHandler(blockRequestHandler);
    }

}
