package com.ymy.cloud.gateway.sentinel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ringo
 * @date 2021/10/10 21:42
 */
@SpringBootApplication
public class GatewaySentinelApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewaySentinelApplication.class, args);
    }
}
