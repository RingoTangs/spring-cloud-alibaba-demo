package com.ymy.cloud.gateway.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Ringo
 * @date 2021/10/10 11:43
 */
@EnableDiscoveryClient
@SpringBootApplication
public class GatewayRegistryNacosApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayRegistryNacosApplication.class, args);
    }
}
