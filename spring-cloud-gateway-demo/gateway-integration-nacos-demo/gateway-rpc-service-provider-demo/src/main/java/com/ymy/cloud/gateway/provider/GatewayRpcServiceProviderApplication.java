package com.ymy.cloud.gateway.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Ringo
 * @date 2021/10/10 11:29
 */
@EnableDiscoveryClient
@SpringBootApplication
public class GatewayRpcServiceProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayRpcServiceProviderApplication.class, args);
    }
}
