package com.ymy.cloud.alibaba.seata.stock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ymy.cloud.alibaba.seata.stock.entity.Stock;
import org.apache.ibatis.annotations.Param;

/**
 * @author Ringo
 */
public interface StockMapper extends BaseMapper<Stock> {

    /**
     * 根据 <code>productId</code> 扣减库存
     *
     * @param productId 商品 id
     * @param reduceNum 扣减库存数量
     * @return 更新数据库影响行数
     */
    Integer reduceStock(@Param("productId") Integer productId, @Param("reduceNum") Integer reduceNum);

}




