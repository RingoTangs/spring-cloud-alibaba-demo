package com.ymy.cloud.alibaba.seata.stock.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ymy.cloud.alibaba.seata.stock.entity.Stock;
import com.ymy.cloud.alibaba.seata.stock.service.StockService;
import com.ymy.cloud.alibaba.seata.stock.mapper.StockMapper;
import org.springframework.stereotype.Service;

/**
 * @author Ringo
 */
@Service
public class StockServiceImpl extends ServiceImpl<StockMapper, Stock>
        implements StockService {

}




