package com.ymy.cloud.alibaba.seata.stock.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ymy.cloud.alibaba.seata.stock.entity.Stock;
import com.ymy.cloud.alibaba.seata.stock.mapper.StockMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Ringo
 * @date 2021/10/9 14:25
 */
@RestController
public class StockController {

    @Resource
    private StockMapper stockMapper;

    @GetMapping("/getStock")
    public Stock getStock(@RequestParam("productId") Integer productId) {
        QueryWrapper<Stock> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId);
        return stockMapper.selectOne(wrapper);
    }

    @GetMapping("/reduce")
    public boolean reduceStock(@RequestParam("productId") Integer productId,
                               @RequestParam("reduceNum") Integer reduceNum) {
        Integer result = stockMapper.reduceStock(productId, reduceNum);
        return result > 0;
    }

}
