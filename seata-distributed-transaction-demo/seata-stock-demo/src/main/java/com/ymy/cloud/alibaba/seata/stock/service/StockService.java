package com.ymy.cloud.alibaba.seata.stock.service;

import com.ymy.cloud.alibaba.seata.stock.entity.Stock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Ringo
 */
public interface StockService extends IService<Stock> {

}
