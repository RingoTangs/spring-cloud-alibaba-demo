package com.ymy.cloud.alibaba.seata.stock;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Ringo
 * @date 2021/10/9 13:07
 */
@MapperScan("com.ymy.cloud.alibaba.seata.stock.mapper")
@EnableDiscoveryClient
@SpringBootApplication
public class SeataStockApplication {
    public static void main(String[] args) {
        SpringApplication.run(SeataStockApplication.class, args);
    }
}
