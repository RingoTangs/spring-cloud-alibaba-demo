package com.ymy.cloud.alibaba.seata.order.controller;

import com.ymy.cloud.alibaba.seata.order.entity.Order;
import com.ymy.cloud.alibaba.seata.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ringo
 * @date 2021/10/9 14:03
 */
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/add")
    public String add(@RequestBody Order order) {
        return orderService.create(order);
    }

}
