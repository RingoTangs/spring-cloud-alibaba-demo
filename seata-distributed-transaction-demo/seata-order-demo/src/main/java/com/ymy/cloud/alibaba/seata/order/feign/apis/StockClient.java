package com.ymy.cloud.alibaba.seata.order.feign.apis;

import com.ymy.cloud.alibaba.seata.stock.entity.Stock;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 库存服务
 *
 * @author Ringo
 * @date 2021/10/9 15:19
 */
@FeignClient("seata-stock-demo")
public interface StockClient {

    /**
     * 查库存
     *
     * @param productId 商品 id
     * @return
     */
    @GetMapping("/getStock")
    Stock getStock(@RequestParam("productId") Integer productId);

    /**
     * 减库存
     *
     * @param productId 商品 id
     * @param reduceNum
     * @return
     */
    @GetMapping("/reduce")
    boolean reduceStock(@RequestParam("productId") Integer productId,
                        @RequestParam("reduceNum") Integer reduceNum);

}
