package com.ymy.cloud.alibaba.seata.order.service;

import com.ymy.cloud.alibaba.seata.order.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Ringo
 * @date 2021/10/9
 */
public interface OrderService extends IService<Order> {

    /**
     * 创建订单
     *
     * @param order 订单
     * @return 提示消息
     */
    String create(Order order);
}
