package com.ymy.cloud.alibaba.seata.order.mapper;

import com.ymy.cloud.alibaba.seata.order.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author Ringo
 * @see com.ymy.cloud.alibaba.seata.order.entity.Order
 */
public interface OrderMapper extends BaseMapper<Order> {

}




