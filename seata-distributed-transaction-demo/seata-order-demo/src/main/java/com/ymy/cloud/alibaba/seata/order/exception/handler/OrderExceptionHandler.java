package com.ymy.cloud.alibaba.seata.order.exception.handler;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Ringo
 * @date 2021/10/9 17:12
 */
@RestControllerAdvice
public class OrderExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public String runtimeExceptionHandler(RuntimeException e) {
        return e.getMessage();
    }

}
