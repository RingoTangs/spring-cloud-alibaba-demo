package com.ymy.spring.cloud.sentinel.openfeign.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.PipedReader;
import java.util.concurrent.TimeUnit;

/**
 * @author Ringo
 * @date 2021/10/6 21:34
 */
@RestController
public class HelloController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/hello")
    public String hello() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500L);
        return "hello " + port;
    }

}
