package com.ymy.spring.cloud.sentinel.openfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Ringo
 * @date 2021/10/6 21:32
 */
@EnableDiscoveryClient
@SpringBootApplication
public class SentinelOpenfeignProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(SentinelOpenfeignProviderApplication.class, args);
    }
}
