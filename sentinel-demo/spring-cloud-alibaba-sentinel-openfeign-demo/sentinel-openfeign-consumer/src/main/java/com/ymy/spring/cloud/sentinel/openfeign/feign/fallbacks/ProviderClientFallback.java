package com.ymy.spring.cloud.sentinel.openfeign.feign.fallbacks;

import com.ymy.spring.cloud.sentinel.openfeign.feign.apis.ProviderClient;
import org.springframework.stereotype.Component;

/**
 * Openfeign 服务降级实现 Feign 的接口即可。
 * 记住要将实现类加入到 IoC 容器
 *
 * @author Ringo
 * @date 2021/10/6 21:56
 */
@Component
public class ProviderClientFallback implements ProviderClient {
    @Override
    public String hello() {
        return "openfeign 熔断降级";
    }
}
