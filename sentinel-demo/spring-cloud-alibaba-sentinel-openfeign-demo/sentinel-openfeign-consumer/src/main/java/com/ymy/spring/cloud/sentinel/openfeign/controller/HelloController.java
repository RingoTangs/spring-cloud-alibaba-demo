package com.ymy.spring.cloud.sentinel.openfeign.controller;

import com.ymy.spring.cloud.sentinel.openfeign.feign.apis.ProviderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ringo
 * @date 2021/10/6 21:47
 */
@RestController
public class HelloController {

    @Value("${server.port}")
    private String port;

    @Resource
    private ProviderClient providerClient;

    @GetMapping("/hello")
    public Map<String, String> hello() {
        Map<String, String> map = new HashMap<>();
        map.put("port", port);
        String result = providerClient.hello();
        result += "99999";
        map.put("result", result);
        return map;
    }

}
