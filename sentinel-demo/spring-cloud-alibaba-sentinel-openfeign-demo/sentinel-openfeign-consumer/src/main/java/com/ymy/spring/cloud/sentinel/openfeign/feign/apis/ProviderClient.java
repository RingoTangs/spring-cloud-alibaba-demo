package com.ymy.spring.cloud.sentinel.openfeign.feign.apis;

import com.ymy.spring.cloud.sentinel.openfeign.feign.fallbacks.ProviderClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Ringo
 * @date 2021/10/6 21:46
 */
@FeignClient(value = "sentinel-openfeign-provider", fallback = ProviderClientFallback.class)
public interface ProviderClient {

    @GetMapping("/hello")
    String hello();

}
