package com.ymy.spring.cloud.sentinel.openfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author Ringo
 * @date 2021/10/6 21:42
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class SentinelOpenfeignConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SentinelOpenfeignConsumerApplication.class, args);
    }
}
