package com.ymy.spring.cloud.alibaba.sentinel.persistent.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * nacos config center 持久化 流控规则 接口
 *
 * @author Ringo
 * @date 2021/10/8 10:07
 */
@RestController
public class HelloController {

    /**
     * 流控规则
     */
    @GetMapping("/hello")
    @SentinelResource(value = "hello",
            blockHandler = "helloFlowRule")
    public String hello() {
        return "hello";
    }


    public String helloFlowRule(BlockException ex) {
        return "限流";
    }

    /**
     * 熔断降级规则
     */
    @GetMapping("/book/{id}")
    @SentinelResource(value = "book",
            blockHandler = "bookCircuitBreaker")
    public String book(@PathVariable("id") String id) throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        return "西游记" + "\t" + id;
    }

    public String bookCircuitBreaker(String id, BlockException ex) {
        return "熔断降级";
    }

    /**
     * 系统规则测试
     */
    @GetMapping("/product")
    public String product() {
        return "product";
    }

}
