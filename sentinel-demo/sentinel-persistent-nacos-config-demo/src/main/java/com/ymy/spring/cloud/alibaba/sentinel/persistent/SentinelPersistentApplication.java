package com.ymy.spring.cloud.alibaba.sentinel.persistent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ringo
 * @date 2021/10/8 10:04
 */
@SpringBootApplication
public class SentinelPersistentApplication {
    public static void main(String[] args) {
        SpringApplication.run(SentinelPersistentApplication.class, args);
    }
}
