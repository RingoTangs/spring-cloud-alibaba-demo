package com.ymy.cloud.alibaba.sentinel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ringo
 * @date 2021/10/6 12:20
 */
@SpringBootApplication
public class CloudAlibabaSentinelDemo {
    public static void main(String[] args) {
        SpringApplication.run(CloudAlibabaSentinelDemo.class, args);
    }
}
