package com.ymy.cloud.alibaba.sentinel.controller.flow.mode;

import com.ymy.cloud.alibaba.sentinel.exception.BlockExceptionHandlerImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 流控模式: 关联
 * 适用场景: 下单接口 触发 查询接口的限流。
 * 这里使用到了 {@link BlockExceptionHandlerImpl}
 *
 * @author Ringo
 * @date 2021/10/6 15:26
 */
@RestController
public class RelationFlowModeController {

    /**
     * 查询接口
     */
    @GetMapping("/get")
    public String get() {
        return "查询接口";
    }

    /**
     * 下单接口
     */
    @GetMapping("/add")
    public String add() {
        return "下单接口";
    }
}
