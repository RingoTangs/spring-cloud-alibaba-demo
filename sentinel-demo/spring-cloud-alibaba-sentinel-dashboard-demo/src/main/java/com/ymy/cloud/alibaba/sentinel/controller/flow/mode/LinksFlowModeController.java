package com.ymy.cloud.alibaba.sentinel.controller.flow.mode;

import com.ymy.cloud.alibaba.sentinel.service.LinksFlowModeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 流控模式：链路
 *
 * @author Ringo
 * @date 2021/10/6 15:55
 */
@RestController
public class LinksFlowModeController {

    @Autowired
    private LinksFlowModeService service;

    @GetMapping("/test1")
    public String test1() {
        return service.getUser();
    }

    @GetMapping("/test2")
    public String test2() {
        return service.getUser();
    }

}
