package com.ymy.cloud.alibaba.sentinel.controller.flow.effect;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 流控效果: warm up 预热
 * @author Ringo
 * @date 2021/10/6 18:40
 */
@RestController
public class WarmUpController {

    @GetMapping("/product")
    public String product() {
        return "product";
    }

}
