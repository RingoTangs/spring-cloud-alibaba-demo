package com.ymy.cloud.alibaba.sentinel.controller.flow;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * 流控测试
 *
 * @author Ringo
 * @date 2021/10/6 12:22
 */
@RestController
public class FlowTestController {


    // 1. QPS: Query Per Second 流控测试
    // 具体的流控规则在 sentinel dashboard 中配置.
    @GetMapping("/hello")
    @SentinelResource(value = "hello",
            blockHandler = "helloFlowControl")
    public String hello() {
        return "hello";
    }

    // 流控降级回调
    public String helloFlowControl(BlockException ex) {
        return "被流控了";
    }

    // =======================================================================
    // 2. 线程数流控测试
    @GetMapping("/book")
    @SentinelResource(value = "book",
            blockHandler = "bookFlowControl")
    public String book() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        return "西游记";
    }

    public String bookFlowControl(BlockException ex) {
        return "流控";
    }


}
