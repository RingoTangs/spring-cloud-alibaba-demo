package com.ymy.cloud.alibaba.sentinel.controller.hotspot;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 热点规则：必须使用 {@link SentinelResource}
 * 热点参数的类型:
 * <li>int</li>
 * <li>double</li>
 * <li>String</li>
 * <li>long</li>
 * <li>float</li>
 * <li>char</li>
 * <li>byte</li>
 *
 * @author Ringo
 * @date 2021/10/7 21:42
 */
@RestController
public class HotspotController {

    @GetMapping("/get/{id}")
    @SentinelResource(value = "getById",
            blockHandler = "hotspotBlockHandler")
    public String getById(@PathVariable("id") String id) {
        return "正常访问";
    }

    public String hotspotBlockHandler(String id, BlockException ex) {
        return "热点异常处理";
    }
}
