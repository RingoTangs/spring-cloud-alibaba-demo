package com.ymy.cloud.alibaba.sentinel.controller.breaker;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * 熔断降级案例
 *
 * @author Ringo
 * @date 2021/10/6 20:28
 */
@RestController
public class CircuitBreakerController {

    // 测试慢调用接口
    @GetMapping("/slowRT")
    @SentinelResource(value = "slowRT",
            blockHandler = "slowRTBlockHandler")
    public String slowRT() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(300L);
        return "slowRT";
    }

    public String slowRTBlockHandler(BlockException ex) {
        return "慢调用比服务熔断降级";
    }

    // 测试异常比例
    @GetMapping("/exceptionRatio")
    @SentinelResource(value = "exceptionRatio",
            blockHandler = "exceptionRatioBlockHandler")
    public String exceptionRatio() {
        throw new RuntimeException();
    }


    public String exceptionRatioBlockHandler(BlockException ex) {
        return "异常比例服务熔断降级";
    }
}
