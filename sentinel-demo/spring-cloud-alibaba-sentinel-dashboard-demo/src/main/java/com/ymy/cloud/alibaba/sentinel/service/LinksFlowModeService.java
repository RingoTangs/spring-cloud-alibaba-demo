package com.ymy.cloud.alibaba.sentinel.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.stereotype.Service;

/**
 * 流控规则：链路模式
 *
 * @author Ringo
 * @date 2021/10/6 15:56
 */
@Service
public class LinksFlowModeService {

    /**
     * 对 Service 进行调用链路的流量控制
     * 需要加 {@link SentinelResource} 注解
     */
    @SentinelResource(value = "getUser",
            blockHandler = "blockExceptionForGetUser")
    public String getUser() {
        return "user zs";
    }

    // 参数一定要加 BlockException
    public String blockExceptionForGetUser(BlockException ex) {
        return "流控";
    }

}
