package com.ymy.cloud.alibaba.sentinel.controller.flow;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 全局 {@link BlockExceptionHandler} 异常处理测试。
 * 服务降级不需要添加 {@link SentinelResource} 注解了。
 *
 * @author Ringo
 * @date 2021/10/6 14:41
 */
@RestController
public class GlobalBlockExceptionHandlerTestController {

    @GetMapping("/goods")
    public String goods() {
        return "goods";
    }

}
