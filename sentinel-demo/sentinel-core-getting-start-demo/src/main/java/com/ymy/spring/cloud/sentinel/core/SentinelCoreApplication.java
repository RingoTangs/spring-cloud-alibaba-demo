package com.ymy.spring.cloud.sentinel.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ringo
 * @date 2021/10/5 17:00
 */
@SpringBootApplication
public class SentinelCoreApplication {
    public static void main(String[] args) {
        SpringApplication.run(SentinelCoreApplication.class, args);
    }
}
