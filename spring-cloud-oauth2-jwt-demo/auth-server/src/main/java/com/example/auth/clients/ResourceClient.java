package com.example.auth.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Ringo
 * @date 2021/10/19 20:05
 */
@FeignClient("resource-server")
public interface ResourceClient {

    @GetMapping("/hello")
    String hello();

}
