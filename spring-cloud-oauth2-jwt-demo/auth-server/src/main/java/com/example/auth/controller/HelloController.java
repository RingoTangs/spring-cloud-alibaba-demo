package com.example.auth.controller;

import com.example.auth.clients.ResourceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Ringo
 * @date 2021/10/19 20:07
 */
@RestController
public class HelloController {

    @Autowired
    private ResourceClient resourceClient;

    @GetMapping("/hello")
    public String hello() {
        return "resource-server return result: " + resourceClient.hello();
    }

}
