package com.example.resource.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

/**
 * {@link PreAuthorize} 需要手动开启
 * <p>
 * {@link RolesAllowed} 需要手动开启 JSR205 注解
 * <p>
 * 开启位置: {@link EnableGlobalMethodSecurity}
 *
 * @author Ringo
 * @date 2021/10/19 20:03
 */
@RestController
public class HelloController {

    @PreAuthorize(value = "#oauth2.hasAnyScope('APPLICATION')")
    @GetMapping("/hello")
    public String hello() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return "hello" + "\t" + principal;
    }

    @RolesAllowed(value = {"ADMIN"})
    @GetMapping("/hello1")
    public String hello1() {
        return "hello1";
    }

}
