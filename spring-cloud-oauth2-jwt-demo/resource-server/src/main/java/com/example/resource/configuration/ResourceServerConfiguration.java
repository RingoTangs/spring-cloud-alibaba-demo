package com.example.resource.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ringo
 * @date 2021/10/19 20:39
 */
@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true, jsr250Enabled = true)
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    public static final String SIGNING_KEY = "RingoTangs";

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(SIGNING_KEY);
        return converter;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId("microservice").tokenStore(tokenStore());
        resources.accessDeniedHandler((request, response, ex) -> {
            response.setContentType("application/json;charset=UTF-8");
            response.setStatus(HttpStatus.SC_FORBIDDEN);
            PrintWriter out = response.getWriter();
            Map<String, String> map = new HashMap<>();
            map.put("message", "拒绝访问");
            map.put("info", ex.getMessage());
            out.print(new ObjectMapper().writeValueAsString(map));
            out.flush();
        });
        resources.authenticationEntryPoint((request, response, ex) -> {
            response.setContentType("application/json;charset=UTF-8");
            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            PrintWriter out = response.getWriter();
            Map<String, String> map = new HashMap<>();
            map.put("message", "校验错误");
            map.put("info", ex.getMessage());
            out.print(new ObjectMapper().writeValueAsString(map));
            out.flush();
        });
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().permitAll();
    }
}
