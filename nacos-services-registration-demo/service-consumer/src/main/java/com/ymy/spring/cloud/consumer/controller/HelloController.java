package com.ymy.spring.cloud.consumer.controller;

import com.ymy.spring.cloud.consumer.feigns.api.ProviderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ringo
 * @date 2021/10/3 17:16
 */
@RestController
public class HelloController {

    @Value("${spring.application.name}")
    private String serviceName;

    @Autowired
    private ProviderClient providerClient;

    @GetMapping("/hello")
    public Map<String, String> hello() {
        Map<String, String> map = new HashMap<>();
        map.put("serviceName", serviceName);
        map.put("RPC result:", providerClient.hello());
        return map;
    }

}
