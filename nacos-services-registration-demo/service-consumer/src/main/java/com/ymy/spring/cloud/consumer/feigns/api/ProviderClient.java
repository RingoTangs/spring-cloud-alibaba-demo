package com.ymy.spring.cloud.consumer.feigns.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 对 <code>service-provider</code> 服务的远程调用
 *
 * @author Ringo
 * @date 2021/10/3 17:31
 */
@FeignClient("service-provider")
public interface ProviderClient {

    @GetMapping("/hello")
    String hello();

}
