package com.ymy.spring.cloud.config.center.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Ringo
 * @date 2021/10/4 19:51
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ConfigCenterProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConfigCenterProviderApplication.class, args);
    }
}
