package com.ymy.spring.cloud.config.center.provider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Nacos 自动刷新配置测试
 * <p>
 * {@link Value} 和 {@link RefreshScope} 需要连用才能实现 nacos config 配置自动更新
 *
 * @author Ringo
 * @date 2021/10/4 21:26
 */
@RefreshScope   // 实现配置自动更新
@RestController
public class HelloController {

    @Value("${person.name}")
    private String name;

    @GetMapping("/hello")
    public String hello() {
        return name;
    }
}
