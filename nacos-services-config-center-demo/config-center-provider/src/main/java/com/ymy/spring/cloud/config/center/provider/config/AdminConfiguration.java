package com.ymy.spring.cloud.config.center.provider.config;

import com.ymy.spring.cloud.config.center.provider.properties.AdminProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ringo
 * @date 2021/10/5 14:32
 */
@Configuration
@EnableConfigurationProperties(value = {AdminProperties.class})
public class AdminConfiguration {
}
