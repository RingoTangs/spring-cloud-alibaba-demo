package com.ymy.spring.cloud.config.center.provider.controller;

import com.ymy.spring.cloud.config.center.provider.properties.AdminProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ringo
 * @date 2021/10/5 14:34
 */
@RestController
public class AdminController {

    @Autowired
    private AdminProperties adminProperties;

    @GetMapping("/admin")
    public Object admin() {
        return adminProperties;
    }


}
