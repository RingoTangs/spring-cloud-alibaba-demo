package com.ymy.spring.cloud.config.center.provider.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * {@link ConfigurationProperties} 配置绑定使用 外部配置、共享配置、spring.application.name 的默认配置,
 * nacos config 更新, 应用都会自动更新！
 *
 * @author Ringo
 * @date 2021/10/5 14:29
 */
@ConfigurationProperties(prefix = "admin.config")
public class AdminProperties {

    private String username = "username";

    private String password = "password";

    @Override
    public String toString() {
        return "AdminProperties{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
