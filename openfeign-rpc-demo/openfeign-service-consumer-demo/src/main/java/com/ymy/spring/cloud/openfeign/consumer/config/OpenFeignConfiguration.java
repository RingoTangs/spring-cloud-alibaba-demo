package com.ymy.spring.cloud.openfeign.consumer.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ringo
 * @date 2021/10/4 12:21
 */
// 全局配置：使用 @Configuration 会开启 OpenFeign 日志管理全局配置
// 局部配置：不要加 @Configuration
@Configuration
public class OpenFeignConfiguration {

    // 配置OpenFeign的日志, 它的日志需要在 debug 环境下才能开启
    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

}
