package com.ymy.spring.cloud.openfeign.consumer.controller;

import com.ymy.spring.cloud.openfeign.consumer.feign.apis.FeignProviderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ringo
 * @date 2021/10/4 11:44
 */
@RestController
public class HelloController {

    @Value("${server.port}")
    private String port;

    @Autowired
    private FeignProviderClient providerClient;

    @GetMapping("/hello")
    public Map<String, String> hello() {
        Map<String, String> map = new HashMap<>();
        map.put("consumer port", port);
        map.put("rpc result", providerClient.hello());
        return map;
    }

}
