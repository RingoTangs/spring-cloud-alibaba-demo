package com.ymy.spring.cloud.openfeign.consumer.feign.interceptors;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 自定义  Feign Request Interceptors
 *
 * @author Ringo
 * @date 2021/10/4 18:56
 */
public class CustomFeignRequestInterceptor implements RequestInterceptor {

    Logger logger = LoggerFactory.getLogger(CustomFeignRequestInterceptor.class);

    @Override
    public void apply(RequestTemplate template) {
        template.header("key", "value");
        System.out.println(template.queryLine());
        logger.info("open feign 请求拦截器");
    }
}