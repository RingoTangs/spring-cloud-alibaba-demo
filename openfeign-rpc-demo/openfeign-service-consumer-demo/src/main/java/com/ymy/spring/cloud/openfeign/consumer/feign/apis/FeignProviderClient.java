package com.ymy.spring.cloud.openfeign.consumer.feign.apis;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Ringo
 * @date 2021/10/4 11:53
 */
@FeignClient("openfeign-service-provider")
public interface FeignProviderClient {

    @GetMapping("/hello")
    String hello();

}
