package com.ymy.spring.cloud.openfeign.provider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Ringo
 * @date 2021/10/4 11:43
 */
@RestController
public class HelloController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/hello")
    public String hello(HttpServletRequest request) {
        // TimeUnit.SECONDS.sleep(3);
        String header = request.getHeader("key");
        return "provider port: " + port + " " + header;
    }

}
